# Author: Fabio Morea @ Area Science Park
# Package: GreenCodes version 1.0
# Description: python program to expand a list of "Green Patent" based on IPC classification, 
#               using classification published by WIPO, and check it against codes available in PATSTAT on line.
#               Methodology and examples are available in 
#               "Green patents and green codes: how different methodologies lead to different results" 
#               Marinella Favot, Leyla Vesnic, Riccardo Priore, Andrea Bincoletto, Fabio Morea
#               Area Science Park, Padriciano 99 34149, Trieste, Italy

# SPDX-License-Identifier: CC-BY-4.0

# GitLab: https://gitlab.com/fabio-morea-areasciencepark/greencodes 


# load libraries
import sys
import pandas as pd 
import re

def expand_codes(filename1,filename2,filenameR, echo=False):
        
    if echo: print("Loading data form ", filename1)
    raw_data = pd.read_excel(filename1, dtype=str)
    print(raw_data.head(5))
    pass 
    raw_data = raw_data[[ "TOPIC", "IPC"]]
    raw_data = raw_data.dropna(subset = ["IPC"])
    
    if echo: print("Loading data form ", filename2)
    codes = pd.read_excel(filename2, dtype=str)
    codes = list(codes["ipc_class_symbol"])
    codes = sorted(codes)

    # scan eaach row of the dataframe to generate a list of results
    # "," is the separator for lists of codes
    # "-" is the separator for sequences
    # and add individual results in a codes_list[]

    codes_list = []
    i=0
    topics_list = []

    for _, row in raw_data.iterrows():
        if str(row['TOPIC']) != "nan": topic = str(row['TOPIC'])
        
        parts = row['IPC'].split(" ")
        first_part  = parts[0]
        for p in range(0,len(parts)):
            parts[p] = re.sub("\xa0"," ",parts[p]) 
            if parts[p][-1]==",": 
                parts[p] = parts[p][:-1]
                
        if echo: print("PARTS: ",parts, len(parts))
        
        if len(parts) == 1:
            if echo: print("this is a short code ", parts[0])
            i+=1
            codes_list.append(parts[0])
            topics_list.append(topic)

        else:
            other_parts=[]
            for p in range(0,len(parts)):
                other_parts = other_parts + parts[p].split(",")
                    
            if echo: print("PROCESSING the parts ",other_parts,len(other_parts))
            for j in range(0,len(other_parts)):
                if echo: print("FOR loop ", j, other_parts[j])
                this_part = re.sub(r'\s+', " ",other_parts[j])# replace non-breakable spaces
                this_part = re.sub(" ", "",this_part) #remove whitespaces
                this_part = re.sub('"',"",this_part) # remove any quotes
                
                full_code = first_part + this_part
                
                if (len(this_part))==0:
                    if echo: print("this part is EMPTY!", full_code)
                elif not("-" in this_part):
                    i+=1
                    codes_list.append(full_code)
                    topics_list.append(topic)
                    if echo: print("Adding a single code ", full_code)
                
                else:#add a sequence
                    sequence_start = first_part + this_part.split("-")[0]
                    sequence_end   = first_part + this_part.split("-")[1]
                    if echo: print("** This is a sequence from ", sequence_start, " to " , sequence_end)
                    # scan the full list of codes and add everything from sequence_start to sequence_end
                    get_also_next_code = False
                    found_any_code = False
                    for cod in codes:
                        if (cod == sequence_start): 

                            if echo: print("ADDING the sequence start",cod)
                            i+=1
                            found_any_code = True
                            codes_list.append(cod)
                            topics_list.append(topic)
                            get_also_next_code = True
                        elif (cod == sequence_end): 
                            if echo: print("ADDING end of the sequence end",cod)
                            i+=1
                            found_any_code = True
                            codes_list.append(cod)
                            topics_list.append(topic)
                            get_also_next_code = False
                            break #don't need to search in the rest of the list
                        elif (get_also_next_code==True):
                            if echo: print("ADDING A PART OF the sequence",cod)
                            i+=1
                            found_any_code = True
                            codes_list.append(cod)
                            topics_list.append(topic)
                    if found_any_code == False: print("WARNING no codes found for the given sequence")
    
    df=pd.DataFrame({'code':codes_list,'topic':topics_list})    
    df = df.sort_values(by=["code", "topic"])            
    #manage an exception: some codes are duplicated such as "H01MH01M"
    for index,row in df.iterrows():
        ccc = row['code']
        if len(ccc)==8:
            if (ccc[:4]==ccc[4:]):
                df = df.drop(index)
                if echo: print("Removed ", ccc)
    
    df = df.drop_duplicates(subset=["code"])
    if echo: print("Duplicates removed")
    
    # write the results to a CSV file
    df.to_csv(filenameR, index=False, header=False)
      
    return(df.shape[0])
     
# script starts here ##############################################

# run a preliminary test on sample data
filename1 = "./test/test_IPC_list.xlsx"
filename2 = "./test/test_IPC_WIPO_unione.xlsx"
filenameR = "./test/test_results.csv"            

test = expand_codes(filename1,filename2,filenameR,echo=False) 

assert test == 170 , "Preliminary test failed"

print("Preliminary test passed")

# run script of data sources given by parameters 1 and 2
assert len(sys.argv) == 4, "This script requires 3 arguments"
filename1 = sys.argv[1]
filename2 = sys.argv[2]
filenameR = sys.argv[3]

n_codes = expand_codes(filename1,filename2,filenameR, echo=False)
print("Process completed. ",n_codes, " codes found.")
print("Check *results* folder ", filenameR)


# final ckeck
results = pd.read_csv(filenameR, dtype=str, header=None, names=["code","_"])
results = list(results["code"])
final_test = pd.read_csv("./test/test.csv", dtype=str, header=1, names=["code"])
 
for _,c in final_test.iterrows():
    if c["code"] in results: 
        pass#print("ok", c)
    else:
        print("NO",c)


