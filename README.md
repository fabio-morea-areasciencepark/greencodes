# OLD VERSION of script to expand a list of "Green Patent" codes, according to IPC and ENVTECH.

full methodology and examples are available in 
"Green patents and green codes: how different methodologies lead to different results"
Marinella Favot, Leyla Vesnic, Riccardo Priore, Andrea Bincoletto, Fabio Morea,  Area Science Park, Padriciano 99 34149, Trieste, Italy
SPDX-License-Identifier: CC-BY-4.0

Please note that this is not the latest version
For latest version see:  https://gitlab.com/area-science-park-sustainability/green-patents
